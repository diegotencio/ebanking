import React from 'react'
import {
  BrowserRouter as Router, 
  Switch,
  Route,
  withRouter,
  Routes
} from 'react-router-dom'
// import AbrirCuenta from './components/AbrirCuenta/AbrirCuenta';
import LandingPageHeader from './components/LandingPage/Header/Header';
import LandingPageFooter from './components/LandingPage/Footer/Footer';
import Login from './components/Login/Login';
import LandingPage from './components/LandingPage/LandingPage';
import MiCuenta from './components/MiCuenta/MiCuenta';
// import OlvidarContraseña from './components/OlvidarContraseña/OlvidarContraseña';
// import Movimientos from './components/Movimientos/Movimientos';


import './App.css';

function App() {
  return (
    <Router>
      <Switch>        
        <Route exact path='/' component={withRouter(LandingPage)}>
          <LandingPageHeader />
          <LandingPage />
          <br></br><br></br><br></br><br></br>
          <LandingPageFooter />
        </Route>
        <Route path='/IniciarSesion'>
          <Login />
        </Route>
        <Route path='/MiCuenta'>
          <MiCuenta />
        </Route>
        {/* <Route path='/AbrirCuenta'>
          <AbrirCuenta />
        </Route>
        <Route path='/OlvidarContraseña'>
          <OlvidarContraseña />
        </Route>
        <Route path='/Movimientos'>
          <Movimientos />
        </Route> */}
      </Switch>
    </Router>
);
}

export default App;
