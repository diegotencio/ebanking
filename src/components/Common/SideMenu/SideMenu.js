import { useState } from "react";
import { AiOutlineMenu, AiOutlineLogout } from "react-icons/ai";
import { FaGem, FaHeart } from "react-icons/fa";
import { FiLogOut} from "react-icons/fi";
import {
  Menu,
  MenuItem,
  ProSidebar,
  SidebarHeader,
  SubMenu,
} from "react-pro-sidebar";
import "react-pro-sidebar/dist/css/styles.css";
import { Link } from "react-router-dom";
import {BsBank2} from "react-icons/bs";

const SideNavigation = () => {
  const [collapsed, setCollapsed] = useState(false);

  // added styles 
  const styles = {
    sideBarHeight: {
      height: "100vh",
    },
    menuIcon: {
      //float: "right",
      //margin: "10px",
    },
  };
  const onClickMenuIcon = () => {
    setCollapsed(!collapsed);
  };
  return (
    <ProSidebar style={styles.sideBarHeight} collapsed={collapsed} className="text-white">
      <SidebarHeader className="text-white">
        <div>
        <BsBank2></BsBank2>&nbsp;
        </div>
        
       
        <div style={styles.menuIcon} onClick={onClickMenuIcon}>
          <AiOutlineMenu />
        </div>
      </SidebarHeader>
      <Menu>
      <SubMenu title="Mi Cuenta" icon={<FaHeart />} className="text-white">
          <MenuItem icon={<FaGem />} >Deposito</MenuItem>
          <MenuItem>Retiro</MenuItem>
        </SubMenu>
        <MenuItem icon={<FaGem />} className="text-white"> Mi Perfil</MenuItem>
        <SubMenu title="Transacciones" icon={<FaHeart />} className="text-white">
          <MenuItem className="text-white">Movimientos</MenuItem>
          <MenuItem className="text-white">Pagos</MenuItem>
          <MenuItem className="text-white">Transferencias</MenuItem>
        </SubMenu>
        <SubMenu title="Account" icon={<FaHeart />} className="text-white">
          <MenuItem icon={<FaGem />}className="text-white" >Permissions</MenuItem>
          <MenuItem className="text-white">Settings</MenuItem>
        </SubMenu>
        <SubMenu title="Email" icon={<FaHeart />} className="text-white">
          <MenuItem className="text-white">Notification</MenuItem>
        </SubMenu>
        <hr></hr>
        <MenuItem icon={<FiLogOut />} className="text-white"> Cerrar Sesion</MenuItem>
      </Menu>
    </ProSidebar>
  );
};

export default SideNavigation;