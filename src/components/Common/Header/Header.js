import React from "react";
import { FaUserCircle } from "react-icons/fa";
import {
  Nav,
  Navbar,
  NavbarBrand,
  NavbarText,
  NavItem,
  NavLink,
} from "reactstrap";

const Header = () => {
  return (
    <>
      <Navbar color="danger" light expand="md">
       
        <NavbarBrand className="text-white"> Banco BDT</NavbarBrand>
       
        <NavbarText>
        <Nav className="mr-auto" navbar>
          {/* <NavItem>
            <NavLink href="/components/" className="text-white">Components</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="https://github.com/reactstrap/reactstrap" className="text-white">
              GitHub
            </NavLink>
          </NavItem> */}
         <NavItem>
            <NavbarText className="text-white">Bienvenido: </NavbarText>
            <NavbarText className="text-white">( NOMBRE DE USUARIO ) </NavbarText>
          </NavItem>         
        </Nav>
          
        </NavbarText>
      </Navbar>
    </>
  );
};

export default Header;