import React from 'react'
import image1 from '../../assets/image-1.jpg'
import './LandingPage.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import LandingPageHeader from '../LandingPage/Header/Header';
import LandingPageFooter from '../LandingPage/Footer/Footer';
import { Link } from 'react-router-dom';
import './LandingPage.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const LandingPage  = (props) => {
  return (
    <div>
      <section className='showcase'>
        <div className='showcase-overlay text-black'>
          <h1 className='text-black'>Bienvenido a nuestro Banco BDT</h1>         
        </div>
      </section>
      <br></br><br></br><br></br><br></br>
      <section className='servicios text-center'>
        <h1><strong>Nuestros Servicios</strong></h1>
        <br></br>
        <Row  style={{width: '100%' }}>
          <Col>
          <img src={image1} alt='destination-1' className='rounded-circle'width="180" height="200"/>            
            <Link className='links' to='/'>
              <h3>Ver información &raquo;</h3>          
            </Link>  
          </Col>
          <Col>
          <img src={image1} alt='destination-1' className='rounded-circle'width="180" height="200"/>            
            <Link className='links' to='/'>
              <h3>Ver información &raquo;</h3>          
            </Link>  
          </Col>
          <Col>
          <img src={image1} alt='destination-1' className='rounded-circle'width="180" height="200"/>            
            <Link className='links' to='/'>
              <h3>Ver información &raquo;</h3>          
            </Link>  
          </Col>
          <Col>
          <img src={image1} alt='destination-1' className='rounded-circle'width="180" height="200"/>            
            <Link className='links' to='/'>
              <h3>Ver información &raquo;</h3>          
            </Link>  
          </Col> 
        </Row>      
      </section>
    </div>


  )
}

export default LandingPage