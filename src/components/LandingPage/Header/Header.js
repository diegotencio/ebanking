import React from 'react'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { Col, Row, Container } from 'react-bootstrap';
import Login from '../../../components/Login/Login';

import './Header.css';


const LandingPageHeader = () => {
  return (
    <header className='header'>
      {/* <Row>
        <Col>
          <Link className='links' to='/'>
            <h1>Banco Diego Tencio</h1>
          </Link>
        </Col>
        <Col>
          <nav className='navbar'>
            <ul>
              <Link className='links' to='/signup'>
                Abrir Cuenta
              </Link>
            </ul>
            <ul>
              <Link className='links' to='/login'>
                Mi Banco
              </Link>
            </ul>
          </nav>
        </Col>
      </Row> */}

      <Container>
        <Row className="justify-content-md-center VerticalAlign">
          <Col>
            <Link className='links' to='/'>
              <h1>Banco Diego Tencio</h1>
            </Link></Col>
          <Col md="auto" className=''>
            {/* <Link className='links btnRadius' to='/AbrirCuenta'>
              <button className="text-white w-100 btn btn-md btn-primary btn-bckground" type="button">Abrir Cuenta</button>
            </Link> */}
          </Col>
          <Col md="auto" className=''>
            <Link className='links btnRadius' to="/IniciarSesion">
              <button className="text-white w-100 btn btn-md btn-primary btn-bckground " type="button">  Mi Banco</button>
            </Link>
          </Col>
        </Row>
      </Container>



      {/* <div>
        <Link className='links' to='/'>
          <h1>Banco Diego Tencio</h1>
          
        </Link>
      </div> */}

      {/* <nav className='navbar'>
        <ul>
          <Link className='links' to='/signup'>
            Abrir Cuenta
          </Link>
          <Link className='links' to='/login'>
            Mi Banco
          </Link>
        </ul>
      </nav> */}
    </header>
    // <div style={{ display: 'block' }}>
    // <Row>
    //   <Col style={{
    //     backgroundColor: 'red',
    //   }}>
    //    <Link className='links' to='/'>
    //        <h1>Banco Diego Tencio</h1>          
    //    </Link>
    // </Col>
    //   <Col style={{
    //     backgroundColor: 'yellow',
    //   }}>
    //     Sample Second Col
    // </Col>
    //   <Col style={{
    //     backgroundColor: 'green',
    //   }}>
    //     <nav className='navbar'>
    //      <ul>
    //       <Link className='links text-white text-center' to='/signup'>
    //        Abrir Cuenta
    //        </Link>
    //        </ul>
    //        <ul>
    //        <Link className='links text-white text-center' to='/login'>
    //          Mi Banco.
    //        </Link>
    //      </ul>
    //    </nav>
    // </Col>
    // </Row>
    // </div>


  )
}

export default LandingPageHeader