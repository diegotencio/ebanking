import Header from "../Common/Header/Header";
import SideNavigation from "../Common/SideMenu/SideMenu";
import Footer from "../Common/Footer/Footer";
import { Col, Row, Container } from 'react-bootstrap';

function MiCuenta() {
  const styles = {
    contentDiv: {
      display: "flex",
    },
    contentMargin: {
      marginLeft: "10px",
      width: "100%",
    },
  };
  return (
    <>
    <Header></Header>
        <div style={styles.contentDiv}>
        <SideNavigation></SideNavigation>
        <div style={styles.contentMargin}>
          <h1 style={{ padding: "20%" }}>CONTENIDO DE MI CUENTA</h1>
        </div>
      </div>
    </>
  );
}

export default MiCuenta;