import React from 'react'
import './Login.css';
import { Link } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

const Login = () => {
  return (
    <div className='background text-center'>
      <header>
        <nav className='navbar navbar-expand-md navbar-dark fixed-top'>
          <div className='container-fluid'>
            <Link className='links' to='/'>
              <i className='text-white fas fa-circle-arrow-left fa-2x'></i>
            </Link>
            <Link className='links' to='/'>
              <h2 className='text-white'>Banco Diego Tencio</h2>
            </Link>
          </div>
        </nav>
      </header>
      <main className="form-signin">
        <div className="p-5 mb-4 rounded-3">
          <div className="container">
            <form>
              <div className='row'>
                <strong>
                  <h1 className="text-white h3 mb-3 fw-normal">Iniciar Sesión</h1>
                </strong>
              </div>
              <i className="text-white fa-solid fa-user-lock fa-7x"></i>
              <br /><br /><br /><br />
              <div className="form-floating">
                <input type="text" className="form-control" id="floatingInput" placeholder="Identificación" />
                <label for="floatingInput">Identificación</label>
              </div>
              <br /><br />
              <div className="form-floating">
                <input type="password" className="form-control" id="floatingPassword" placeholder="Contraseña" />
                <label for="floatingPassword">Contraseña</label>
              </div>
              <br /><br />
              <div className="row mb-3">
                <div className="col-6">
                  <Link className='links text-white' to='/MiCuenta'>
                    <button className="text-white w-100 btn btn-md btn-primary btn-bckground" type="button">Iniciar Sesión</button>
                  </Link>

                </div>
                <div className="col-6">
                  <Link className='links text-white' to='/OlvidarContraseña'>
                    ¿Olvidó su contraseña?
                  </Link>
                </div>
              </div>
              <hr className="text-white" />
              <p className="text-white ">&copy; Diego Tencio Hdz &middot; 2022 </p>
            </form>
          </div>
        </div>
      </main>


    </div>
  )
}

export default Login